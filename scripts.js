const clientes = [
    {
        dni: "11111111A",
        nombre: "Juan Luis",
        apellidos: "Pérez Subirats",
        saldoMedio: 2435.33,
        saldoMaximo: 4530.00,
        nomina: true,
        telefonos: {
            movil: "654654654",
            fijo: "932932932"
        }
    },
    {
        dni: "22222222B",
        nombre: "Mª del Mar",
        apellidos: "Romero Escalante",
        saldoMedio: 1201.04,
        saldoMaximo: 3000.00,
        nomina: true,
        telefonos: {
            movil: "612612612",
            fijo: ""
        }
    },
    {
        dni: "33333333C",
        nombre: "Antoni",
        apellidos: "Pereira Ponce",
        saldoMedio: 12347.49,
        saldoMaximo: 25312.00,
        nomina: false,
        telefonos: {
            movil: "668668668",
            fijo: "938938938"
        }
    },
    {
        dni: "44444444D",
        nombre: "Idoia",
        apellidos: "Llach Estevez",
        saldoMedio: 1904.88,
        saldoMaximo: 4611.50,
        nomina: false,
        telefonos: {
            movil: "694694694",
            fijo: ""
        }
    },
    {
        dni: "55555555E",
        nombre: "Carlos",
        apellidos: "González Prieto",
        saldoMedio: 353.15,
        saldoMaximo: 1500,
        nomina: true,
        telefonos: {
            movil: "631631631",
            fijo: "934934934"
        }
    },
    {
        dni: "66666666F",
        nombre: "Cristina",
        apellidos: "Pulido Calero",
        saldoMedio: 1671.22,
        saldoMaximo: 3955.15,
        nomina: true,
        telefonos: {
            movil: "649649649",
            fijo: "939939939"
        }
    }
];

